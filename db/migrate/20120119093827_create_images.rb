class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :title
      t.text :description
      t.string :state
      t.string :image
      t.integer :album_id

      t.timestamps
    end
  end
end
