PhotoArchive::Application.routes.draw do
  scope module: :web do
    root to: "welcome#index"
    resource :users, only: [:new, :create] 
    resource :sessions, only: [:new, :create, :destroy]
    resource :albums, only: [:show, :index]
    namespace :admin do 
      root to: "welcome#index"
      resources :albums, except: :show 
      resources :images, except: :show 
    end
  end
end
