module Web::Admin::AlbumsHelper
  def albums_options
    options = []
    Album.all.each do |i|
      options << [i.title, i.id]
    end
    options
  end
end
