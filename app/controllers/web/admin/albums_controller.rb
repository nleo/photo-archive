class Web::Admin::AlbumsController < Web::Admin::ApplicationController
  def index
    @web_admin_albums = Album.all
  end

  def show
    @web_admin_album = Album.find(params[:id])
  end

  def new
    @web_admin_album = Album.new
  end

  def edit
    @web_admin_album = Album.find(params[:id])
  end

  def create
    @web_admin_album = Album.new(params[:album])
    if @web_admin_album.save
      redirect_to admin_albums_path, notice: 'Album was successfully created.'
    else
      render action: "new" 
    end
  end

  def update
    @web_admin_album = Album.find(params[:id])

    if @web_admin_album.update_attributes(params[:album])
      redirect_to admin_albums_path, notice: 'Album was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @web_admin_album = Album.find(params[:id])
    @web_admin_album.destroy
    redirect_to admin_albums_url 
  end
end
