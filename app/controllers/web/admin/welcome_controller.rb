class Web::Admin::WelcomeController < Web::Admin::ApplicationController
  def index
    @images = Image.limit 10
    @albums = Album.limit 10
  end
end
