class User < ActiveRecord::Base
  has_secure_password
  attr_accessible :email, :password, :password_confirmation  
  validates :email, presence: true
  validates :password, presence: true, on: :create
  has_many :images
end
