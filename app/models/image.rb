class Image < ActiveRecord::Base
  validates :image, presence: true
  validates :album_id, presence: true
  belongs_to :album
  mount_uploader :image, ImageUploader
end
