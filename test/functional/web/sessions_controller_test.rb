require 'test_helper'

class Web::SessionsControllerTest < ActionController::TestCase
  test "user can sing in" do
    user = Factory :user
    post :create, :email => user.email, :password => user.password
    assert_response :redirect
    assert signed_in?
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get destroy" do
    get :destroy
    assert_response :redirect
    assert_nil signed_in?
  end

end