require 'test_helper'

class Web::Admin::WelcomeControllerTest < ActionController::TestCase
  def setup
    @user = Factory :user
    sign_in(@user)
  end

  test "should be success" do
    get :index
    assert_response :success
  end

end
