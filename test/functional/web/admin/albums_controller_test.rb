require 'test_helper'

class Web::Admin::AlbumsControllerTest < ActionController::TestCase
  def setup
    @user = Factory :user
    sign_in(@user)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    attrs = Factory.attributes_for(:album)
    post :create, :album => attrs
    assert_response :redirect
    album = Album.find_by_title(attrs[:title])
    assert album
  end

  test "should get edit" do
    album = Factory :album
    get :edit, :id => album.id
    assert_response :success
  end

  test "should be updated" do
    album = Factory :album
    attrs = Factory.attributes_for :album
    get :update, :id => album.id, :album => attrs
    assert_response :redirect
  end

  test "should get destroy" do
    album = Factory :album
    delete :destroy, :id => album.id
    assert_response :redirect
    album = Album.find_by_id(album.id)
    assert_nil album
  end

end
