require 'test_helper'

class Web::Admin::ImagesControllerTest < ActionController::TestCase
  def setup
    @user = Factory :user
    sign_in(@user)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    attrs = Factory.attributes_for(:image)    
    post :create, image: attrs
    assert_response :redirect
    image = Image.find_by_title(attrs[:title])
    assert image
  end

  test "should get edit" do
    image = Factory :image
    get :edit, id: image.id
    assert_response :success
  end

  test "should be updated" do
    image = Factory :image
    attrs = Factory.attributes_for :image
    get :update, id: image.id, :image => attrs
    assert_response :redirect
  end

  test "should get destroy" do
    image = Factory :image
    delete :destroy, id: image.id
    assert_response :redirect
    image = Image.find_by_id(image.id)
    assert_nil image
  end

end
