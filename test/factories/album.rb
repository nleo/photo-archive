FactoryGirl.define do
  factory :album do
    title {FactoryGirl.generate :string}
    description {FactoryGirl.generate :string}
  end
end