include ActionDispatch::TestProcess
FactoryGirl.define do  
  factory :image do 
    title {FactoryGirl.generate :string}
    description {FactoryGirl.generate :string}
    image { fixture_file_upload(Rails.root+"test/samples/image.jpg", "image/jpeg") }
    album_id {Factory.create(:album).id}
  end
end