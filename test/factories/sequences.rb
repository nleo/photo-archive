FactoryGirl.define do
   sequence :email do |n|
     "email-#{n}@mail.com"
   end

   sequence :string do |n|
     "string-#{n}"
   end

   sequence :name do |n|
     "name-#{n}"
   end
end
